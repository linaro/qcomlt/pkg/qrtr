qrtr (0.3+47g9dc7a88-1) unstable; urgency=medium

   * Fix versioning to be compatible with debian using git describe.
     For example v0.3-47-g9dc7a88 will be transform to 0.3+47g9dc7a88.

 -- Aníbal Limón <anibal.limon@linaro.org>  Fri, 29 Jan 2021 10:57:23 -0600

qrtr (0.3-47-g9dc7a88) unstable; urgency=medium

   * Update versioning, get using "git describe --match v0.* --tags" on
     upstream revision.

   Changes,

   * 9dc7a88 libqrtr: Zero-initialize sockaddr_qrtr

 -- Aníbal Limón <anibal.limon@linaro.org>  Fri, 29 Jan 2021 10:25:42 -0600

qrtr (0.6-983b223423f3ec-1) unstable; urgency=medium

  New upstream version,

  Changes,

  * 983b223 (origin/upstream, bjorn/master, upstream) lookup: Add TFTP to known services list
  * cb1a647 qrtr-lookup: Add DPM service name
  * 47e48a8 Android: Disable "treat warnings as errors".
  * 33bf949 Clang: fix for arithmetic disallowed on void pointers
  * fef484b libqrtr: Use size_t for offsets
  * 2ed52a3 qrtr-ns.service: do not install as executable

 -- Aníbal Limón <anibal.limon@linaro.org>  Wed, 02 Dec 2020 15:01:19 -0600

qrtr (0.5+7bd5cf30243775-1) sid; urgency=medium

  * ns: Go dormant when exsting name server is found
  * ANDROID: Add Android.bp makefile

 -- Aníbal Limón <anibal.limon@linaro.org>  Wed, 11 Mar 2020 18:09:15 -0600

qrtr (0.4+cd6bedd5d00f21-1) buster; urgency=medium

  * qrtr-ns.service: Add systemd service
  * map: Fix removal of entries from the table
  * logging: add min priority and other cleanup
  * qrtr-lookup: print better information about DIAG service

 -- Aníbal Limón <anibal.limon@linaro.org>  Thu, 01 Aug 2019 22:12:23 -0500

qrtr (0.3+1ga1694a1c938f-1) buster; urgency=medium

  [ Eric Caruso ]
  * qmi: remove unused include
  * ns: alphabetize includes
  * lookup: alphabetize includes
  * cfg, lookup, ns: avoid using __progname
  * cfg, ns: factor out qrtr_set_address function
  * Unify logging into one function
  * logging: add use_syslog to qlog_setup
  * ns: add -s option for logging to syslog

  [ Ben Chan ]
  * lookup: fix service name for service type 17

  [ Bjorn Andersson ]
  * qrtr-lookup: Add description of service 52

  [ Ben Chan ]
  * libqrtr: add extern "C" guard in libqrtr.h
  * qrtr-ns: initialize waiter_ticket struct in waiter_wait_timeout()

  [ Arun Kumar Neelakantam ]
  * libqrtr : Add support for signed one byte enum.

  [ Ben Chan ]
  * qmi: pass `const struct qrtr_packet' to qmi_decode_header()

  [ Bjorn Andersson ]
  * libqrtr: Correct "instance" composition

 -- Nicolas Dechesne <nicolas.dechesne@linaro.org>  Tue, 07 Aug 2018 07:47:43 +0200

qrtr (0.0+2g7d9a2e7-1) buster; urgency=medium

  [ Bjorn Andersson ]
  * cfg: Trigger kernel module autoloading
  * ns: Allow qrtr-ns to configure node id
  * ns: Add argument to stay in foreground

  [ Nicolas Dechesne ]
  * debian: qrtr.service: remove modprobe workaround

 -- Nicolas Dechesne <nicolas.dechesne@linaro.org>  Fri, 20 Apr 2018 12:23:08 +0200

qrtr (0.0+1g539c83d-2) buster; urgency=medium

  * debian: enable systemd packaging

 -- Nicolas Dechesne <nicolas.dechesne@linaro.org>  Thu, 19 Apr 2018 08:59:31 +0200

qrtr (0.0+1g539c83d-1) buster; urgency=medium

  [ Bjorn Andersson ]
  * qrtr-cfg: Add to .gitignore
  * qrtr-lookup: Make output easier to read
  * qrtr: ns: Don't broadcast reset command
  * qrtr: Hide build warning
  * ns: Track services per node
  * ns: Extract NS message handling into separate functions
  * ns: Refactore control message handling
  * ns: Store local node id in context
  * ns: Add static bcast_sq to context
  * make: Link non-static
  * ns: Advertise new and removed services
  * ns: Cleanup debug and error prints
  * ns: Move qrtr_ctrl_cmd struct to ns.h
  * lib: Provide new API for clients to register services
  * ns: Implement DEL_CLIENT
  * ns: Make del_client always propagate del_client
  * ns: Handle BYE control message
  * lib: Replace qrtr_publish() and qrtr_bye() implementations
  * ns: Introduce lookup registration
  * lookup: Transition to using control port
  * ns: Remove the ns service
  * ns: Rename context socket
  * ns: Cleanup socket creation
  * cfg: Request and check return value
  * lib: Add helpers for handling control messages
  * ns: Replace lookup result with new and del server messages
  * lib: Fix endian handling and add new/del server
  * lookup: Report service, version and instance separate
  * lookup: Extend service name table
  * qrtr-ns: Propagate the node in BYE
  * libqrtr: Don't require socket to be bound for lookup
  * qrtr-ns: Move DEL_SERVER logic to server_del
  * qrtr-ns: Sanity check origin of messages
  * lookup: Add DIAG service
  * libqrtr: Don't enforce server port to be bound
  * qrtr: Backport future qrtr.h uapi
  * libqrtr: Add qrtr_decode() operation
  * qrtr: Drop container.h
  * qmi: Introduce QMI encoder/decoder helpers
  * libqrtr: Rename libqrtr.c for the purpose of symmetry
  * libqrtr: Remove depricated functions
  * Makefile: Add lib to include path of qrtr-cfg

  [ Ben Chan ]
  * libqrtr: define AF_QIPCRTR if it isn't defined in libc header

  [ Nicolas Dechesne ]
  * qrtr.service: ensure qrtr modules are loaded

 -- Nicolas Dechesne <nicolas.dechesne@linaro.org>  Wed, 18 Apr 2018 13:58:27 +0200

qrtr (0.0+0+9c85f35a-1) stretch; urgency=medium

  * Makefile: add standard GNU makefiles directory variables
  * debian: update debian/* to support multiarch

 -- Nicolas Dechesne <nicolas.dechesne@linaro.org>  Tue, 29 Nov 2016 09:54:05 +0100

qrtr (0.0+0+56074bd5-2) jessie; urgency=medium

  * debian: add qrtr systemd service script

 -- Nicolas Dechesne <nicolas.dechesne@linaro.org>  Fri, 10 Jun 2016 13:56:07 +0200

qrtr (0.0+0+56074bd5-1) jessie; urgency=low

  * Initial Release.

 -- Nicolas Dechesne <nicolas.dechesne@linaro.org>  Fri, 03 Jun 2016 12:53:23 +0000
